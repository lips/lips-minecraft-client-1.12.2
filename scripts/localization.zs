val old_vial_name = <extraalchemy:vial_break>.displayName;
<extraalchemy:vial_break>.clearTooltip();
<extraalchemy:vial_break>.displayName = old_vial_name;
<extraalchemy:vial_break>.addTooltip(format.white(old_vial_name));
val vial_info = format.gray("Right click to fill with a splash potion from your inventory or craft together with a splash potion.") + format.white("   ");
<extraalchemy:vial_break>.addTooltip(vial_info);
<extraalchemy:vial_break>.addTooltip(format.yellow(format.bold(format.underline("Extra Alchemy"))));

val old_pot_name = <harvestcraft:potitem>.displayName;
<harvestcraft:potitem>.clearTooltip();
<harvestcraft:potitem>.displayName = old_pot_name;
<harvestcraft:potitem>.addTooltip(format.white(old_pot_name));
<harvestcraft:potitem>.addTooltip(format.yellow(format.bold(format.underline("Pam's HarvestCraft"))));

var old_bullet_name = <elderarsenal:basic_bullet>.displayName;
<elderarsenal:basic_bullet>.clearTooltip();
<elderarsenal:basic_bullet>.displayName = old_bullet_name;
<elderarsenal:basic_bullet>.addTooltip(format.white(old_bullet_name));
<elderarsenal:basic_bullet>.addTooltip(format.yellow(format.bold(format.underline("Elder Arsenal"))));

<minecraft:experience_bottle>.displayName = "Experience Bottle";
<minecraft:lit_pumpkin>.displayName = "Halloween Pumpkin Lantern";
<harvestcraft:potatoesobrienitem>.displayName = "Pan-Fried Potatoes";

game.setLocalization("entity.twilightforest.roving_cube.name", "Roving Cube [NYI]");
game.setLocalization("entity.twilightforest.harbinger_cube.name", "Harbinger Cube [NYI]");

game.setLocalization("quarkmisc.chestButton.dropoff", "Deposit To Nearby Chests");
game.setLocalization("quarkmisc.chestButton.dropoff.shift", "Merge To Nearby Chests");

game.setLocalization("tile.twilightforest.Deadrock.name", "Twilight's Deadrock Block");
