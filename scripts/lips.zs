game.setLocalization("item.lipslibrary.item02.name", "Blocks Helper");
val item02_info = format.gray("When held, provides information about blocks.");
<lipslibrary:item02>.addTooltip(item02_info);

game.setLocalization("item.lipslibrary.item06.name", "Entities Helper");
val item06_info = format.gray("When held, provides information about entities.");
<lipslibrary:item06>.addTooltip(item06_info);

game.setLocalization("item.lipslibrary.item05.name", "Twilight Forest Catalyst");
val item05_info = format.aqua("Drop this thing into a pond surrounded by flowers.");
<lipslibrary:item05>.addTooltip(item05_info);

game.setLocalization("item.lipslibrary.item03.name", "The Ender Material");
val item03_info = format.aqua("This material can be used to produce elytras.");
<lipslibrary:item03>.addTooltip(item03_info);

game.setLocalization("item.lipslibrary.item01.name", "Building Wand");
game.setLocalization("item.lipslibrary.item04.name", "Navigation Wand");
