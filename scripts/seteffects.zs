val leather_set = mods.SetEffect.newSet()
	.setName("leather_set")
	.withHead(<minecraft:leather_helmet>)
	.withChest(<minecraft:leather_chestplate>)
	.withLegs(<minecraft:leather_leggings>)
	.withFeet(<minecraft:leather_boots>)
	.addEffect(<potion:minecraft:speed>.makePotionEffect(10, 0));

mods.SetEffect.register(leather_set);
val leather_set_info = format.gold("Set Effect:") + " " + format.white("20% Walking Speed");
<minecraft:leather_helmet:*>.addTooltip(leather_set_info);
<minecraft:leather_chestplate:*>.addTooltip(leather_set_info);
<minecraft:leather_leggings:*>.addTooltip(leather_set_info);
<minecraft:leather_boots:*>.addTooltip(leather_set_info);



val chainmail_set = mods.SetEffect.newSet()
	.setName("chainmail_set")
	.withHead(<minecraft:chainmail_helmet>)
	.withChest(<minecraft:chainmail_chestplate>)
	.withLegs(<minecraft:chainmail_leggings>)
	.withFeet(<minecraft:chainmail_boots>)
	.addEffect(<potion:minecraft:resistance>.makePotionEffect(10, 1));

mods.SetEffect.register(chainmail_set);
val chainmail_set_info = format.gold("Set Effect:") + " " + format.white("40% Damage Reduction");
<minecraft:chainmail_helmet:*>.addTooltip(chainmail_set_info);
<minecraft:chainmail_chestplate:*>.addTooltip(chainmail_set_info);
<minecraft:chainmail_leggings:*>.addTooltip(chainmail_set_info);
<minecraft:chainmail_boots:*>.addTooltip(chainmail_set_info);



val iron_set = mods.SetEffect.newSet()
	.setName("iron_set")
	.withHead(<minecraft:iron_helmet>)
	.withChest(<minecraft:iron_chestplate>)
	.withLegs(<minecraft:iron_leggings>)
	.withFeet(<minecraft:iron_boots>)
	.addEffect(<potion:minecraft:strength>.makePotionEffect(10, 1));

mods.SetEffect.register(iron_set);
val iron_set_info = format.gold("Set Effect:") + " " + format.white("+3.0") + " " + format.red("❤") + " " + format.white("Melee Damage");
<minecraft:iron_helmet:*>.addTooltip(iron_set_info);
<minecraft:iron_chestplate:*>.addTooltip(iron_set_info);
<minecraft:iron_leggings:*>.addTooltip(iron_set_info);
<minecraft:iron_boots:*>.addTooltip(iron_set_info);



val golden_set = mods.SetEffect.newSet()
	.setName("golden_set")
	.withHead(<minecraft:golden_helmet>)
	.withChest(<minecraft:golden_chestplate>)
	.withLegs(<minecraft:golden_leggings>)
	.withFeet(<minecraft:golden_boots>)
	.addEffect(<potion:minecraft:haste>.makePotionEffect(10, 3));

mods.SetEffect.register(golden_set);
val golden_set_info = format.gold("Set Effect:") + " " + format.white("80% Mining Speed + 40% Attack Speed");
<minecraft:golden_helmet:*>.addTooltip(golden_set_info);
<minecraft:golden_chestplate:*>.addTooltip(golden_set_info);
<minecraft:golden_leggings:*>.addTooltip(golden_set_info);
<minecraft:golden_boots:*>.addTooltip(golden_set_info);



val diamond_set = mods.SetEffect.newSet()
	.setName("diamond_set")
	.withHead(<minecraft:diamond_helmet>)
	.withChest(<minecraft:diamond_chestplate>)
	.withLegs(<minecraft:diamond_leggings>)
	.withFeet(<minecraft:diamond_boots>)
	.addEffect(<potion:minecraft:resistance>.makePotionEffect(10, 3));

mods.SetEffect.register(diamond_set);
val diamond_set_info = format.gold("Set Effect:") + " " + format.white("80% Damage Reduction");
<minecraft:diamond_helmet:*>.addTooltip(diamond_set_info);
<minecraft:diamond_chestplate:*>.addTooltip(diamond_set_info);
<minecraft:diamond_leggings:*>.addTooltip(diamond_set_info);
<minecraft:diamond_boots:*>.addTooltip(diamond_set_info);



val constantan_set = mods.SetEffect.newSet()
	.setName("constantan_set")
	.withHead(<thermalfoundation:armor.helmet_constantan>)
	.withChest(<thermalfoundation:armor.plate_constantan>)
	.withLegs(<thermalfoundation:armor.legs_constantan>)
	.withFeet(<thermalfoundation:armor.boots_constantan>)
	.addEffect(<potion:minecraft:speed>.makePotionEffect(10, 2));

mods.SetEffect.register(constantan_set);
val constantan_set_info = format.gold("Set Effect:") + " " + format.white("60% Walking Speed");
<thermalfoundation:armor.helmet_constantan:*>.addTooltip(constantan_set_info);
<thermalfoundation:armor.plate_constantan:*>.addTooltip(constantan_set_info);
<thermalfoundation:armor.legs_constantan:*>.addTooltip(constantan_set_info);
<thermalfoundation:armor.boots_constantan:*>.addTooltip(constantan_set_info);



val bronze_set = mods.SetEffect.newSet()
	.setName("bronze_set")
	.withHead(<thermalfoundation:armor.helmet_bronze>)
	.withChest(<thermalfoundation:armor.plate_bronze>)
	.withLegs(<thermalfoundation:armor.legs_bronze>)
	.withFeet(<thermalfoundation:armor.boots_bronze>)
	.addEffect(<potion:minecraft:speed>.makePotionEffect(10, 3));

mods.SetEffect.register(bronze_set);
val bronze_set_info = format.gold("Set Effect:") + " " + format.white("80% Walking Speed");
<thermalfoundation:armor.helmet_bronze:*>.addTooltip(bronze_set_info);
<thermalfoundation:armor.plate_bronze:*>.addTooltip(bronze_set_info);
<thermalfoundation:armor.legs_bronze:*>.addTooltip(bronze_set_info);
<thermalfoundation:armor.boots_bronze:*>.addTooltip(bronze_set_info);



val invar_set = mods.SetEffect.newSet()
	.setName("invar_set")
	.withHead(<thermalfoundation:armor.helmet_invar>)
	.withChest(<thermalfoundation:armor.plate_invar>)
	.withLegs(<thermalfoundation:armor.legs_invar>)
	.withFeet(<thermalfoundation:armor.boots_invar>)
	.addEffect(<potion:extraalchemy:effect.photosynthesis>.makePotionEffect(10, 0));

mods.SetEffect.register(invar_set);
val invar_set_info = format.gold("Set Effect:") + " " + format.green("Photosynthesis");
<thermalfoundation:armor.helmet_invar:*>.addTooltip(invar_set_info);
<thermalfoundation:armor.plate_invar:*>.addTooltip(invar_set_info);
<thermalfoundation:armor.legs_invar:*>.addTooltip(invar_set_info);
<thermalfoundation:armor.boots_invar:*>.addTooltip(invar_set_info);



val copper_set = mods.SetEffect.newSet()
	.setName("copper_set")
	.withHead(<thermalfoundation:armor.helmet_copper>)
	.withChest(<thermalfoundation:armor.plate_copper>)
	.withLegs(<thermalfoundation:armor.legs_copper>)
	.withFeet(<thermalfoundation:armor.boots_copper>)
	.addEffect(<potion:minecraft:haste>.makePotionEffect(10, 0));

mods.SetEffect.register(copper_set);
val copper_set_info = format.gold("Set Effect:") + " " + format.white("20% Mining Speed + 10% Attack Speed");
<thermalfoundation:armor.helmet_copper:*>.addTooltip(copper_set_info);
<thermalfoundation:armor.plate_copper:*>.addTooltip(copper_set_info);
<thermalfoundation:armor.legs_copper:*>.addTooltip(copper_set_info);
<thermalfoundation:armor.boots_copper:*>.addTooltip(copper_set_info);



val tin_set = mods.SetEffect.newSet()
	.setName("tin_set")
	.withHead(<thermalfoundation:armor.helmet_tin>)
	.withChest(<thermalfoundation:armor.plate_tin>)
	.withLegs(<thermalfoundation:armor.legs_tin>)
	.withFeet(<thermalfoundation:armor.boots_tin>)
	.addEffect(<potion:minecraft:resistance>.makePotionEffect(10, 0));

mods.SetEffect.register(tin_set);
val tin_set_info = format.gold("Set Effect:") + " " + format.white("20% Damage Reduction");
<thermalfoundation:armor.helmet_tin:*>.addTooltip(tin_set_info);
<thermalfoundation:armor.plate_tin:*>.addTooltip(tin_set_info);
<thermalfoundation:armor.legs_tin:*>.addTooltip(tin_set_info);
<thermalfoundation:armor.boots_tin:*>.addTooltip(tin_set_info);



val silver_set = mods.SetEffect.newSet()
	.setName("silver_set")
	.withHead(<thermalfoundation:armor.helmet_silver>)
	.withChest(<thermalfoundation:armor.plate_silver>)
	.withLegs(<thermalfoundation:armor.legs_silver>)
	.withFeet(<thermalfoundation:armor.boots_silver>)
	.addEffect(<potion:minecraft:strength>.makePotionEffect(10, 0));

mods.SetEffect.register(silver_set);
val silver_set_info = format.gold("Set Effect:") + " " + format.white("+1.5") + " " + format.red("❤") + " " + format.white("Melee Damage");
<thermalfoundation:armor.helmet_silver:*>.addTooltip(silver_set_info);
<thermalfoundation:armor.plate_silver:*>.addTooltip(silver_set_info);
<thermalfoundation:armor.legs_silver:*>.addTooltip(silver_set_info);
<thermalfoundation:armor.boots_silver:*>.addTooltip(silver_set_info);



val lead_set = mods.SetEffect.newSet()
	.setName("lead_set")
	.withHead(<thermalfoundation:armor.helmet_lead>)
	.withChest(<thermalfoundation:armor.plate_lead>)
	.withLegs(<thermalfoundation:armor.legs_lead>)
	.withFeet(<thermalfoundation:armor.boots_lead>)
	.addEffect(<potion:minecraft:resistance>.makePotionEffect(10, 2));

mods.SetEffect.register(lead_set);
val lead_set_info = format.gold("Set Effect:") + " " + format.white("60% Damage Reduction");
<thermalfoundation:armor.helmet_lead:*>.addTooltip(lead_set_info);
<thermalfoundation:armor.plate_lead:*>.addTooltip(lead_set_info);
<thermalfoundation:armor.legs_lead:*>.addTooltip(lead_set_info);
<thermalfoundation:armor.boots_lead:*>.addTooltip(lead_set_info);



val aluminum_set = mods.SetEffect.newSet()
	.setName("aluminum_set")
	.withHead(<thermalfoundation:armor.helmet_aluminum>)
	.withChest(<thermalfoundation:armor.plate_aluminum>)
	.withLegs(<thermalfoundation:armor.legs_aluminum>)
	.withFeet(<thermalfoundation:armor.boots_aluminum>)
	.addEffect(<potion:minecraft:speed>.makePotionEffect(10, 1));

mods.SetEffect.register(aluminum_set);
val aluminum_set_info = format.gold("Set Effect:") + " " + format.white("40% Walking Speed");
<thermalfoundation:armor.helmet_aluminum:*>.addTooltip(aluminum_set_info);
<thermalfoundation:armor.plate_aluminum:*>.addTooltip(aluminum_set_info);
<thermalfoundation:armor.legs_aluminum:*>.addTooltip(aluminum_set_info);
<thermalfoundation:armor.boots_aluminum:*>.addTooltip(aluminum_set_info);



val nickel_set = mods.SetEffect.newSet()
	.setName("nickel_set")
	.withHead(<thermalfoundation:armor.helmet_nickel>)
	.withChest(<thermalfoundation:armor.plate_nickel>)
	.withLegs(<thermalfoundation:armor.legs_nickel>)
	.withFeet(<thermalfoundation:armor.boots_nickel>)
	.addEffect(<potion:minecraft:haste>.makePotionEffect(10, 1));

mods.SetEffect.register(nickel_set);
val nickel_set_info = format.gold("Set Effect:") + " " + format.white("40% Mining Speed + 20% Attack Speed");
<thermalfoundation:armor.helmet_nickel:*>.addTooltip(nickel_set_info);
<thermalfoundation:armor.plate_nickel:*>.addTooltip(nickel_set_info);
<thermalfoundation:armor.legs_nickel:*>.addTooltip(nickel_set_info);
<thermalfoundation:armor.boots_nickel:*>.addTooltip(nickel_set_info);



val platinum_set = mods.SetEffect.newSet()
	.setName("platinum_set")
	.withHead(<thermalfoundation:armor.helmet_platinum>)
	.withChest(<thermalfoundation:armor.plate_platinum>)
	.withLegs(<thermalfoundation:armor.legs_platinum>)
	.withFeet(<thermalfoundation:armor.boots_platinum>)
	.addEffect(<potion:extraalchemy:effect.leech>.makePotionEffect(10, 0));

mods.SetEffect.register(platinum_set);
val platinum_set_info = format.gold("Set Effect:") + " " + format.darkRed("Lifesteal");
<thermalfoundation:armor.helmet_platinum:*>.addTooltip(platinum_set_info);
<thermalfoundation:armor.plate_platinum:*>.addTooltip(platinum_set_info);
<thermalfoundation:armor.legs_platinum:*>.addTooltip(platinum_set_info);
<thermalfoundation:armor.boots_platinum:*>.addTooltip(platinum_set_info);



val steel_set = mods.SetEffect.newSet()
	.setName("steel_set")
	.withHead(<thermalfoundation:armor.helmet_steel>)
	.withChest(<thermalfoundation:armor.plate_steel>)
	.withLegs(<thermalfoundation:armor.legs_steel>)
	.withFeet(<thermalfoundation:armor.boots_steel>)
	.addEffect(<potion:minecraft:strength>.makePotionEffect(10, 2));

mods.SetEffect.register(steel_set);
val steel_set_info = format.gold("Set Effect:") + " " + format.white("+4.5") + " " + format.red("❤") + " " + format.white("Melee Damage");
<thermalfoundation:armor.helmet_steel:*>.addTooltip(steel_set_info);
<thermalfoundation:armor.plate_steel:*>.addTooltip(steel_set_info);
<thermalfoundation:armor.legs_steel:*>.addTooltip(steel_set_info);
<thermalfoundation:armor.boots_steel:*>.addTooltip(steel_set_info);



val electrum_set = mods.SetEffect.newSet()
	.setName("electrum_set")
	.withHead(<thermalfoundation:armor.helmet_electrum>)
	.withChest(<thermalfoundation:armor.plate_electrum>)
	.withLegs(<thermalfoundation:armor.legs_electrum>)
	.withFeet(<thermalfoundation:armor.boots_electrum>)
	.addEffect(<potion:minecraft:haste>.makePotionEffect(10, 2));

mods.SetEffect.register(electrum_set);
val electrum_set_info = format.gold("Set Effect:") + " " + format.white("60% Mining Speed + 30% Attack Speed");
<thermalfoundation:armor.helmet_electrum:*>.addTooltip(electrum_set_info);
<thermalfoundation:armor.plate_electrum:*>.addTooltip(electrum_set_info);
<thermalfoundation:armor.legs_electrum:*>.addTooltip(electrum_set_info);
<thermalfoundation:armor.boots_electrum:*>.addTooltip(electrum_set_info);
