val bag = <improvedbackpacks:backpack>.withTag({
	display: {Name: "Starting Bag"},
	Items: [
		{Slot: 0, id: "davincisvessels:securedbed", Count: 1},
		{Slot: 1, id: "immersiveengineering:tool", Count: 1, Damage: 3},
		{Slot: 2, id: "botania:lexicon", Count: 1},
		{Slot: 3, id: "harvestcraft:steakandchipsitem", Count: 8},
		{Slot: 4, id: "harvestcraft:garlicchickenitem", Count: 8},
		{Slot: 5, id: "minecraft:torch", Count: 32},
		{Slot: 6, id: "nolives:heart", Count: 2},
		{Slot: 7, id: "minecraft:experience_bottle", Count: 8},
		{Slot: 8, id: "waystones:waystone", Count: 2},
		{Slot: 9, id: "improvableskills:skills_book", Count: 1},
		{Slot: 10, id: "cfm:item_recipe_book", Count: 1},
		{Slot: 11, id: "astralsorcery:itemjournal", Count: 1},
		{Slot: 12, id: "harvestcraft:bbqplatteritem", Count: 8},
		{Slot: 13, id: "harvestcraft:soymilkitem", Count: 8},
		{Slot: 14, id: "potionfingers:ring", Count: 1, tag: {display: {Name: "Ring of Learning"}, effect: "extraalchemy:effect.learning"}, Damage: 1},
		{Slot: 15, id: "immersiveengineering:earmuffs", Count: 1},
		{Slot: 16, id: "minecraft:clock", Count: 1},
		{Slot: 17, id: "sereneseasons:season_clock", Count: 1}
	]});

mods.initialinventory.InvHandler.addStartingItem(bag);
