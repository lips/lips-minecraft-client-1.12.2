recipes.remove(<cfm:tv>, false);
recipes.remove(<cfm:modern_tv>, false);

recipes.addShaped(
	<cfm:tv>,
	[
		[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],
		[<minecraft:iron_ingot>, <minecraft:prismarine_crystals>, <minecraft:iron_ingot>],
		[<minecraft:iron_ingot>, <minecraft:redstone>, <minecraft:iron_ingot>]
	]);

recipes.addShaped(
	<cfm:modern_tv>,
	[
		[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],
		[<minecraft:prismarine_crystals>, <minecraft:prismarine_crystals>, <minecraft:prismarine_crystals>],
		[<minecraft:iron_ingot>, <minecraft:redstone>, <minecraft:iron_ingot>]
	]);

furnace.addRecipe(<minecraft:leather>, <minecraft:rotten_flesh>);
furnace.addRecipe(<biomesoplenty:biome_block>, <minecraft:dirt>);

recipes.addShaped(
	<waystones:warp_stone>,
	[
		[<ore:dyePurple>, <minecraft:ender_pearl>, <ore:dyePurple>],
		[<minecraft:ender_pearl>, <minecraft:diamond>, <minecraft:ender_pearl>],
		[<ore:dyePurple>, <minecraft:ender_pearl>, <ore:dyePurple>]
	]);

recipes.addShaped(
	<biomesoplenty:biome_finder>,
	[
		[<ore:treeSapling>, null, <ore:treeSapling>],
		[<minecraft:compass>, <minecraft:lapis_block>, <minecraft:compass>],
		[<ore:treeSapling>, null, <ore:treeSapling>]
	]);

recipes.addShaped(
	<biomesoplenty:biome_finder>,
	[
		[<ore:treeSapling>, <minecraft:compass>, <ore:treeSapling>],
		[null, <minecraft:lapis_block>, null],
		[<ore:treeSapling>, <minecraft:compass>, <ore:treeSapling>]
	]);

val clean_compass = <biomesoplenty:biome_finder>.withEmptyTag();
recipes.addShapeless(clean_compass, [
	<minecraft:water_bucket>.transformReplace(<minecraft:bucket>),
	<biomesoplenty:biome_finder>]);

recipes.addShapeless(
	<thaumicgrid:thaumic_grid>,
	[
		<refinedstorage:grid>,
		<refinedstorage:processor:5>,
		<thaumcraft:arcane_workbench>
	]);

recipes.addShaped(
	<minecraft:elytra>,
	[
		[null, <minecraft:string>, null],
		[<lipslibrary:item03>, <minecraft:stick>, <lipslibrary:item03>],
		[<lipslibrary:item03>, <minecraft:leather>, <lipslibrary:item03>]
	]);

recipes.addShaped(
	<lipslibrary:item03>,
	[
		[null, <minecraft:diamond>, null],
		[<minecraft:diamond>, <minecraft:ender_eye>, <minecraft:diamond>],
		[null, <minecraft:diamond>, null]
	]);

recipes.addShapeless(
	<lipslibrary:item05>,
	[
		<minecraft:end_crystal>,
		<minecraft:ender_eye>,
		<minecraft:diamond>
	]);

recipes.addShapeless("lipsrepairelytra", <minecraft:elytra>, [<minecraft:elytra>.anyDamage().marked("elytra"), <minecraft:diamond>], function (output, ingredients, crafting)
{
	return ingredients.elytra.withDamage(0);
},
null);

recipes.addShapeless("lipsrepairsteelshield", <immersiveengineering:shield>, [<immersiveengineering:shield>.anyDamage().marked("shield"), <thermalfoundation:material:352>], function (output, ingredients, crafting)
{
	return ingredients.shield.withDamage(0);
},
null);
