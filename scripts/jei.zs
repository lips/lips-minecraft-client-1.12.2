import mods.jei.JEI;

JEI.hide(<minecraft:spawn_egg:*>);
JEI.hide(<minecraft:barrier>);
JEI.hide(<minecraft:structure_void>);

JEI.hide(<thaumcraft:taint_fibre>);
JEI.hide(<thaumcraft:enchanted_placeholder>);
JEI.hide(<thaumcraft:barrier>);
JEI.hide(<thaumcraft:hole>);
JEI.hide(<thaumcraft:effect_sap>);
JEI.hide(<thaumcraft:effect_glimmer>);
JEI.hide(<thaumcraft:effect_shock>);
JEI.hide(<thaumcraft:pillar_arcane>);
JEI.hide(<thaumcraft:pillar_eldritch>);
JEI.hide(<thaumcraft:pillar_ancient>);

JEI.hide(<rpsideas:conjuredblock>);
JEI.hide(<rpsideas:conjuredpulsar>);
JEI.hide(<rpsideas:conjuredstar>);
JEI.hide(<rpsideas:conjuredpulsarlight>);

JEI.hide(<thermalexpansion:morb>);
JEI.hide(<thermalexpansion:florb>);

JEI.hide(<refinedstorage:hollow_cover>);
JEI.hide(<refinedstorage:cover>);

JEI.hide(<twilightforest:twilight_plant:*>);
JEI.hide(<twilightforest:cinder_furnace>);
JEI.hide(<twilightforest:cinder_log>);
JEI.hide(<twilightforest:trophy:*>);

JEI.hide(<lipslibrary:item02>);
JEI.hide(<lipslibrary:item06>);
JEI.hide(<lipslibrary:item01>);
JEI.hide(<lipslibrary:item04>);

JEI.hide(<cfm:item_drink>);
JEI.hide(<cfm:item_crayfish>);
JEI.hide(<cfm:ceiling_fan_fans>);

JEI.hide(<psi:conjured>);
JEI.hide(<hammercore:manual>);
JEI.hide(<quark:ancient_tome>);
JEI.hide(<biomesoplenty:earth>);
JEI.hide(<sereneseasons:ss_icon>);
JEI.hide(<astralsorcery:blockportalnode:*>);
